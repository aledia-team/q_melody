

$('.owl-carousel').owlCarousel({
    loop: true,
    margin: 10,
    nav: true,
    autoplay:true,
    autoplayTimeout:3000,
    items: 1,
    navText: ["<div class='left'><span class='arrow-left'></span></div>", "<div class='right'><span class='arrow-right'></span></div>"],
    responsive: {
        0: {
            stagePadding: 20
        },
        769: {
            stagePadding: 0
        }
    }
})



        


        // SCROLL BG COLOR
        $(window).scroll(function() {
          var $window = $(window),
              $body = $('body'),
              $panel = $('section, footer, header');

          var scroll = $window.scrollTop() + ($window.height() / 3);

          $panel.each(function () {
            var $this = $(this);
            if ($this.position().top <= scroll && $this.position().top + $this.height() > scroll) {

              $body.removeClass(function (index, css) {
                return (css.match (/(^|\s)color-\S+/g) || []).join(' ');
              });

              $body.addClass('color-' + $(this).data('color'));
            }
          });    

        }).scroll();
    
// GO TO TOP
            $(window).scroll(function () {
                if ($(this).scrollTop() > 300) {
                    $('.gotop').fadeIn();
                } else {
                    $('.gotop').fadeOut();
                }
            });

            $('.gotop').on('click', function(e) {
                $("html, body").animate({
                    scrollTop: 0
                }, 600);
                return false;
            });




$(document).ready(function()
{
      var $body = $('body');
    $('.nav-link').hover(function()
    {
        $(this).addClass("light-nav-link");
        $('.nav-link').addClass('dark-nav-link');
    }, function()
    {
        $('.nav-link').removeClass("dark-nav-link");
        $(this).removeClass("light-nav-link");
    });



     $('#openBook').click(function () {
        $('.mobile-menu').fadeToggle('fast','linear' );
        $body.addClass('hide-overflow');
        $('.mobile-menu .menu-list').slideToggle('slow','swing' );
        //$('body').addClass('nav-open');
    });
    $('#close-icon').click(function () {
        $('.mobile-menu').fadeToggle('fast','linear' );
        $body.removeClass('hide-overflow');
        $('.mobile-menu .menu-list').slideToggle('slow','swing' );
         
         //$('body').removeClass('nav-open');
    });

   
});